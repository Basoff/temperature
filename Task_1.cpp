#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
using namespace std;

int main()
{
	int year, month, day = 0, temp, add = 0,sday,wday;
	double atemp, stemp, wtemp;
	
	
	ifstream in ("f30823v4.m02");
	if (!in.is_open())
	{
		cout<<"Failed to open the file, sorry :(";
		return -1;
	}
	
	ofstream out ("result.txt");
	if (!out.is_open())
	{
		cout<<"Failed to create an output file, sorry :(";
		return -2;
	}
	char buff[50];

	in >> buff >> buff;
	year = atoi(buff);

	while (true)
	{
		in >> buff;
		month = atoi(buff);
		in >> buff >> buff >> buff >> buff >> buff;
		if(atoi(buff) <= 400 && atoi(buff) >= -400)
			atemp += atoi(buff);
		if (month < 3 || month == 12)
		{
			wday ++;
			wtemp += atoi(buff);
		}
		if (month < 9 && month > 6)
		{
			sday ++;
			stemp += atoi(buff);
		}
		
		in >> buff >> buff >> buff >> buff >> buff >> buff;
		
		day ++;
		
		in >> buff;
		if (buff == "")
		{
			in.close();
			out.close();
			return 0;
		}
		else
		{
			in >> buff;
			add = atoi(buff);
			if (year != add)
			{
				atemp /= day*10; stemp /= sday*10; wtemp /= wday*10;
				cout<<year<<" "<<atemp<<"	"<<wtemp<<" "<<stemp<<endl;
				out<<year<<" "<<atemp<<"	"<<wtemp<<" "<<stemp<<endl;
				year = add;
				atemp = day = stemp = wtemp = 0;
			}
		} 
	}
	
	in.close();
	out.close();
	return 0;
}
